package com.maksoft.phonecounterdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maksoft.phonecounterdemo.service.SwedishMobileMsisdnService;

@SpringBootApplication
public class PhoneCounterDemoApplication implements CommandLineRunner {

    private SwedishMobileMsisdnService swedishMobileMsisdnService;

    @Autowired
    public PhoneCounterDemoApplication(SwedishMobileMsisdnService swedishMobileMsisdnService) {
        this.swedishMobileMsisdnService = swedishMobileMsisdnService;
    }

    public static void main(String[] args) {
        SpringApplication.run(PhoneCounterDemoApplication.class, args);
    }


    @Override
    public void run(String... args) {
        swedishMobileMsisdnService.getSwedishMobileMsisdn();
    }
}
