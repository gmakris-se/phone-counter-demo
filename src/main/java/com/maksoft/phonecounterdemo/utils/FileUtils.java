package com.maksoft.phonecounterdemo.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);

    private FileUtils() {
        // util class
    }

    public static Path createNewOutputFile(String outputFilePath) {
        Path path = Path.of(outputFilePath);

        if (Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                LOGGER.error("Cannot delete the existing file. {}", e.toString());
            }
        }

        try {
            Files.createFile(path);
            return path;
        } catch (IOException e) {
            LOGGER.error("Cannot create new Swedish mobile output file. {}", e.toString());
            return null;
        }

    }

}
