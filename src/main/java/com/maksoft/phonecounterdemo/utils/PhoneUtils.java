package com.maksoft.phonecounterdemo.utils;

import java.util.Optional;

import org.apache.logging.log4j.util.Strings;

public class PhoneUtils {

    private PhoneUtils() {
        // util class
    }

    public static Optional<String> removeNonDigitCharacters(final String input) {
        if (Strings.isNotEmpty(input)) {
            return Optional.of(input.replaceAll("[^\\d]", ""));
        } else {
            return Optional.empty();
        }
    }

}
