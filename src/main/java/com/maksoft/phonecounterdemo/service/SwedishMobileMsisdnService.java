package com.maksoft.phonecounterdemo.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.maksoft.phonecounterdemo.utils.FileUtils;
import com.maksoft.phonecounterdemo.utils.PhoneUtils;

/**
 * A service for Swedish Mobile Numbers
 */
@Service
public class SwedishMobileMsisdnService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwedishMobileMsisdnService.class);

    @Value("#{${sweden.mobile.allowedPrefixes}}")
    private List<String> allowedPrefixesTenDigitsFormat;

    @Value("${sweden.mobile.inputFile}")
    private String swedishMobileInputFile;

    @Value("${sweden.mobile.outputFile}")
    private String swedishMobileOutputFile;

    /**
     * Reads the input file line by line and creates a new file with all valid Swedish mobile phones in MSDNIS format and sorted
     */
    public void getSwedishMobileMsisdn() {
        final Path path = Path.of(swedishMobileInputFile);
        final boolean readable = Files.isReadable(path);

        if (!readable) {
            LOGGER.error("Cannot read the Swedish mobile input file, does it exist? Given path: {}", swedishMobileInputFile);
            return;
        }

        try (final Stream<String> lines = Files.lines(path)) {
            final Path outputPath = FileUtils.createNewOutputFile(swedishMobileOutputFile);
            if (outputPath == null) {
                LOGGER.error("Cannot access the output Path !");
                return;
            }

            final String outputString = convertToMsisdnAndJoin(lines);
            Files.writeString(outputPath, outputString, StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        } catch (IOException e) {
            LOGGER.error("Cannot read the Swedish mobile input file. {}", e.toString());
        }

    }

    /**
     * Trying to convert all phones to MSISDN format and join them in one String in format: MSISDN_Number;counter
     *
     * @param lines Stream with all lines of the file that contain the phones
     * @return a String that contains all the msdisdn phones followed by semicolon and their count
     */
    String convertToMsisdnAndJoin(Stream<String> lines) {
        return lines.map(this::mapToMsisdn)
                .flatMap(Optional::stream)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .map(e -> e.getKey() + ";" + e.getValue())
                .sorted()
                .collect(Collectors.joining("\n"));
    }

    /**
     * Applies the rules to convert a Swedish mobile phone candidate to MSISDN
     *
     * @param phone candidate of a Swedish mobile phone
     * @return the phone in MSISDN, or empty if no rule can be applied
     */
    Optional<String> mapToMsisdn(String phone) {
        final Optional<String> phoneOnlyDigitsOptional = PhoneUtils.removeNonDigitCharacters(phone);
        if (phoneOnlyDigitsOptional.isPresent()) {
            final String phoneOnlyDigits = phoneOnlyDigitsOptional.get();

            if (isInMsisdnFormat(phoneOnlyDigits)) {
                final String result = "+" + phoneOnlyDigits;
                LOGGER.debug("Rule +46 applied. input: {}, output: {}", phoneOnlyDigits, result);
                return Optional.of(result);
            } else if (isInTenDigitsFormat(phoneOnlyDigits)) {
                final String result = "+46" + phoneOnlyDigits.substring(1);
                LOGGER.debug("Rule 10,0 applied. input: {}, output: {}", phoneOnlyDigits, result);
                return Optional.of(result);
            } else if (isInNineDigitsFormat(phoneOnlyDigits)) {
                final String result = "+46" + phoneOnlyDigits;
                LOGGER.debug("Rule 10,7 applied. input: {}, output: {}", phoneOnlyDigits, result);
                return Optional.of(result);
            }
        }

        LOGGER.debug("No rule can be applied. input: {}", phoneOnlyDigitsOptional);
        return Optional.empty();
    }

    /**
     * Checks if the phone has length of 9 and starting with any of the allowed prefixes (without the leading zero)
     *
     * @param phone candidate of a Swedish mobile phone
     * @return true if matches the above rule
     */
    private boolean isInNineDigitsFormat(String phone) {
        return phone.length() == 9 &&
                allowedPrefixesTenDigitsFormat.contains("0" + phone.substring(0, 2));
    }

    /**
     * Checks if the phone has length of 10 and starting with any of the allowed prefixes (including the leading zero)
     *
     * @param phone candidate of a Swedish mobile phone
     * @return true if matches the above rule
     */
    private boolean isInTenDigitsFormat(String phone) {
        return phone.length() == 10 &&
                allowedPrefixesTenDigitsFormat.contains(phone.substring(0, 3));
    }

    /**
     * Checks if the phone has length of 11 and starting with 46 followed by any of the allowed prefixes (without the leading zero)
     *
     * @param phone candidate of a Swedish mobile phone
     * @return true if matches the above rule
     */
    private boolean isInMsisdnFormat(String phone) {
        return phone.length() == 11 &&
                phone.startsWith("46") &&
                allowedPrefixesTenDigitsFormat.contains("0" + phone.substring(2, 4));
    }
}
