package com.maksoft.phonecounterdemo.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class PhoneUtilsTest {

    private static Stream<Arguments> removeNonDigitCharactersData() {
        return Stream.of(
                Arguments.of("null String", null, Optional.empty()),
                Arguments.of("empty String", "", Optional.empty()),
                Arguments.of("spaces only", "  ", Optional.of("")),
                Arguments.of("custom text", "not blank", Optional.of("")),
                Arguments.of("nine digits", "123456789", Optional.of("123456789")),
                Arguments.of("Rule already in MSISDN format 1", "+46738041141", Optional.of("46738041141")),
                Arguments.of("Rule already in MSISDN format 2", "+46-738041141", Optional.of("46738041141")),
                Arguments.of("Rule already in MSISDN format 3", "+46-73 80 41 141", Optional.of("46738041141")),
                Arguments.of("Rule ten digits format with chars", "73s80-41gg14ss1", Optional.of("738041141"))
        );
    }

    @ParameterizedTest(name = "[{index}] - {0}: {1}/{2}")
    @MethodSource("removeNonDigitCharactersData")
    void removeNonDigitCharacters(String description, String input, Optional<String> expected) {
        final Optional<String> result = PhoneUtils.removeNonDigitCharacters(input);
        assertThat(result).isEqualTo(expected);
    }
}