package com.maksoft.phonecounterdemo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SwedishMobileMsisdnServiceTest {

    @Autowired
    private SwedishMobileMsisdnService msisdnService;

    private static Stream<Arguments> mapToMsisdnData() {
        return Stream.of(
                Arguments.of("null String", null, Optional.empty()),
                Arguments.of("empty String", "", Optional.empty()),
                Arguments.of("spaces only", "  ", Optional.empty()),
                Arguments.of("custom string", "not blank", Optional.empty()),
                Arguments.of("None matching rule, 8 digits", "123456789", Optional.empty()),
                Arguments.of("None matching rule, wrong prefix", "123456789", Optional.empty()),
                Arguments.of("Rule already in MSISDN format 1", "+46738041141", Optional.of("+46738041141")),
                Arguments.of("Rule already in MSISDN format 2", "+46-738041141", Optional.of("+46738041141")),
                Arguments.of("Rule already in MSISDN format 3", "+46-73 80 41 141", Optional.of("+46738041141")),
                Arguments.of("Rule ten digits format 1", "0738041141", Optional.of("+46738041141")),
                Arguments.of("Rule ten digits format 2", "07-38041141", Optional.of("+46738041141")),
                Arguments.of("Rule ten digits format 3", "073-8041141", Optional.of("+46738041141")),
                Arguments.of("Rule ten digits format 4", "0738-041141", Optional.of("+46738041141")),
                Arguments.of("Rule nine digits format 1", "738041141", Optional.of("+46738041141")),
                Arguments.of("Rule nine digits format 2", "73-8041141", Optional.of("+46738041141")),
                Arguments.of("Rule nine digits format 3", "738-041141", Optional.of("+46738041141")),
                Arguments.of("Rule nine digits format 4", "7380-41141", Optional.of("+46738041141")),
                Arguments.of("Rule ten digits format with chars", "73s80-41gg14ss1", Optional.of("+46738041141"))
        );
    }

    @ParameterizedTest(name = "[{index}] - {0}: {1}/{2}")
    @MethodSource("mapToMsisdnData")
    void mapToMsisdn(String description, String phone, Optional<String> expected) {
        final Optional<String> result = msisdnService.mapToMsisdn(phone);
        assertThat(result).isEqualTo(expected);
    }

    @Test
    void testConvertToMsisdnAndJoin_1() {
        final Stream<String> lines = Stream.of("+46738041140", "+46738041141", "+46738041142", "+46738041142");

        final String expected = "+46738041140;1\n"
                + "+46738041141;1\n"
                + "+46738041142;2";

        final String actual = msisdnService.convertToMsisdnAndJoin(lines);

        assertThat(actual).isEqualTo(expected);

    }

    @Test
    void testConvertToMsisdnAndJoin_2() {
        final Stream<String> lines = Stream.of("738041140", "+46738041140", "0738041140", "73804114");

        final String expected = "+46738041140;3";

        final String actual = msisdnService.convertToMsisdnAndJoin(lines);

        assertThat(actual).isEqualTo(expected);

    }

    @Test
    void testConvertToMsisdnAndJoin_3() {
        final Stream<String> lines = Stream.of("738041140", "+46738041140", "0738041140", "738041141");

        final String expected = "+46738041140;3\n"
                + "+46738041141;1";

        final String actual = msisdnService.convertToMsisdnAndJoin(lines);

        assertThat(actual).isEqualTo(expected);

    }
}